# log in to your AKS

1. az login (make sure you are logged in with Microsoft edge)
2. az aks install-cli
3. az aks get-credentials --resource-group (for example- "rg-final-project-westEurope") --name (for examle- "aks-final-project-westEurope")
4. kubectl config get-contexts
![code](kubectl-config-get-contexts.png)

